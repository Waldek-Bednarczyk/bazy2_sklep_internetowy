﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for NewMagazinWindow.xaml
    /// </summary>
    public partial class NewMagazinWindow : Window
    {
        public NewMagazinWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if ( produktIDTextBox.Text != "" && nazwa_magazynuTextBox.Text != "" && kod_pocztowyTextBox.Text != "" && krajTextBox.Text != "" && miejscowoscTextBox.Text != ""
                 && numer_budynkuTextBox.Text != "" && numer_lokaluTextBox.Text != "" && ulicaTextBox.Text != "" &&
                wojewodztwoTextBox.Text != "")
            {
                try
                {
                    ((MainWindow)Application.Current.MainWindow)._InternetowyEntities.Tworzenie_magazynu(Convert.ToInt32(produktIDTextBox.Text), nazwa_magazynuTextBox.Text,
                        krajTextBox.Text, wojewodztwoTextBox.Text, miejscowoscTextBox.Text,
                        Convert.ToInt32(kod_pocztowyTextBox.Text), ulicaTextBox.Text, Convert.ToInt32(numer_budynkuTextBox.Text), Convert.ToInt32(numer_lokaluTextBox.Text));
                    MessageBox.Show("Sukces", "Operacja zakończona powodzeniem", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Błąd", ex.InnerException.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
            }
            else MessageBox.Show("Błąd", "Podaj dane", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
