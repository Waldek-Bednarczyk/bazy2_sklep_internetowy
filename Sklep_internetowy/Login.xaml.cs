﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(textBoxLogin.Text != "" && passwordBoxHaslo.Password != "")
            {
                if(textBoxLogin.Text != "admin" && passwordBoxHaslo.Password != "admin")
                {
                    MySqlConnection conn = ((MainWindow)Application.Current.MainWindow).conn;
                    conn.Open();
                    string query = "SELECT UzytkownikID FROM uzytkownicy WHERE Nick_uzytkownika = \"" + textBoxLogin.Text + "\" AND Haslo_uzytkownika = \"" + passwordBoxHaslo.Password + "\"";
                    MySqlCommand comm = new MySqlCommand(query, conn);
                    MySqlDataAdapter adp = new MySqlDataAdapter(comm);
                    DataSet ds = new DataSet();
                    adp.Fill(ds, "LoadDataBinding");
                    conn.Close();
                    if(ds.Tables[0].Rows.Count == 0)
                    {
                        MessageBox.Show("Błędne dane logowania", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        string IDFromDatabase = ds.Tables[0].Rows[0]["UzytkownikID"].ToString();
                        MainWindow main = new MainWindow(Convert.ToInt32(IDFromDatabase));
                        this.Hide();
                        main.ShowDialog();
                        main.Close();
                        textBoxLogin.Text = "";
                        passwordBoxHaslo.Password = "";
                        this.Show();
                    }
                }
                else
                {
                    MainWindow main = new MainWindow(Convert.ToInt32(-1));
                    this.Hide();
                    main.ShowDialog();
                    main.Close();
                    textBoxLogin.Text = "";
                    passwordBoxHaslo.Password = "";
                    this.Show();
                }
            }
            else MessageBox.Show("Błąd", "Podaj dane", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void ButtonNewAcoount_Click(object sender, RoutedEventArgs e)
        {
            NewAccountWindow newAccountWindow = new NewAccountWindow();
            newAccountWindow.Show();
            textBoxLogin.Text = "";
            passwordBoxHaslo.Password = "";
        }
    }
}
