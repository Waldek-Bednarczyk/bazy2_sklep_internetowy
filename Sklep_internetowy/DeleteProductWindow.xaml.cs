﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for DeleteProductWindow.xaml
    /// </summary>
    public partial class DeleteProductWindow : Window
    {
        public DeleteProductWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (textBoxName.Text != "")
            {
                try
                {
                    ((MainWindow)Application.Current.MainWindow)._InternetowyEntities.Usuniecie_produktu(textBoxName.Text);
                    MessageBox.Show("Sukces", "Operacja zakończona powodzeniem", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.InnerException.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
            }
            else MessageBox.Show("Błąd", "Podaj dane", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
