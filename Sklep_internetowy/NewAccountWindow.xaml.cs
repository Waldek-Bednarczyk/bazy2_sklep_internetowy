﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for NewAccountWindow.xaml
    /// </summary>
    public partial class NewAccountWindow : Window
    {
        public NewAccountWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource uzytkownicyViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("uzytkownicyViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // uzytkownicyViewSource.Source = [generic data source]
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(passwordBox.Password != "" && imie_uzytkownikaTextBox1.Text != "" && kod_pocztowyTextBox.Text != "" && krajTextBox.Text != "" && miejscowoscTextBox.Text != "" &&
                nazwisko_uzytkownikaTextBox1.Text != "" && nick_uzytkownikaTextBox1.Text != "" && numer_budynkuTextBox.Text != "" && numer_lokaluTextBox.Text != "" && ulicaTextBox.Text != "" &&
                wojewodztwoTextBox.Text != "")
            {
                try
                {
                    if ((passwordBox.Password.Length >= 8) && passwordBox.Password.Any(char.IsDigit) && passwordBox.Password.Any(char.IsSymbol))
                    {
                        ((MainWindow)Application.Current.MainWindow)._InternetowyEntities.Tworzenie_konta(imie_uzytkownikaTextBox1.Text, nazwisko_uzytkownikaTextBox1.Text,
                        nick_uzytkownikaTextBox1.Text, passwordBox.Password, krajTextBox.Text, wojewodztwoTextBox.Text, miejscowoscTextBox.Text,
                        Convert.ToInt32(kod_pocztowyTextBox.Text), ulicaTextBox.Text, Convert.ToInt32(numer_budynkuTextBox.Text), Convert.ToInt32(numer_lokaluTextBox.Text));
                        MessageBox.Show("Sukces", "Operacja zakończona powodzeniem", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Close();
                    }
                    else MessageBox.Show("Podaj haslo spelniajace wymagania: \nCo najmniej 8 znakow \nCo najmniej 1 cyfra \nCo najmniej 1 znak specjalny", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Błąd", ex.InnerException.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
            }
            else MessageBox.Show("Błąd", "Podaj dane", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
