﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for NewOrderWindow.xaml
    /// </summary>
    public partial class NewOrderWindow : Window
    {
        int _produktID, _price, _ID;

        public NewOrderWindow(int produktID, int price, int ID)
        {
            InitializeComponent();
            _produktID = produktID;
            _price = price;
            _ID = ID;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void ButtonReturn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonOrder_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxQuantity.Text !="")
            {
                try
                {
                    MySqlConnection conn = ((MainWindow)Application.Current.MainWindow).conn;
                    conn.Open();
                    string query = "SELECT PracownikID FROM pracownicy ORDER BY RAND() LIMIT 1";
                    MySqlCommand comm = new MySqlCommand(query, conn);
                    MySqlDataAdapter adp = new MySqlDataAdapter(comm);
                    DataSet ds = new DataSet();
                    adp.Fill(ds, "LoadDataBinding");
                    conn.Close();

                    int sum = Convert.ToInt32(textBoxQuantity.Text) * _price;
                    DateTime date = DateTime.Today;
                    date.AddDays(10);
                    System.Data.Entity.Core.Objects.ObjectParameter objectParameter = new System.Data.Entity.Core.Objects.ObjectParameter("id", typeof(int));
                    string IDEmployeeFromDatabase = ds.Tables[0].Rows[0]["PracownikID"].ToString();
                    ((MainWindow)Application.Current.MainWindow)._InternetowyEntities.Nowe_zamowienie(null,
                        _ID, Convert.ToInt32(IDEmployeeFromDatabase),
                        sum,"W trakcie realizacji",date,false,_produktID,Convert.ToInt32(textBoxQuantity.Text), objectParameter);
                    MessageBox.Show("Sukces", "Operacja zakończona powodzeniem", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Błąd", ex.InnerException.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
            }
            else MessageBox.Show("Błąd", "Podaj dane", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
