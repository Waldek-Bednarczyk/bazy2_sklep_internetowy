﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for ChangePasswordWindow.xaml
    /// </summary>
    public partial class ChangePasswordWindow : Window
    {
        public ChangePasswordWindow()
        {
            InitializeComponent();
        }

        private void ButtonAccept_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxLogin.Text != "" && passBoxLoginNew.Password != "" && passBoxLoginOld.Password != "")
            {
                if((passBoxLoginNew.Password.Length >= 8) && passBoxLoginNew.Password.Any(char.IsDigit) && passBoxLoginNew.Password.Any(char.IsSymbol))
                try
                {
                    ((MainWindow)Application.Current.MainWindow)._InternetowyEntities.Zmiana_Hasla(textBoxLogin.Text, passBoxLoginOld.Password, passBoxLoginNew.Password);
                    MessageBox.Show("Sukces", "Operacja zakończona powodzeniem" , MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
                    catch (Exception ex)
                {   
                    MessageBox.Show(ex.InnerException.Message,"Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
                else MessageBox.Show("Podaj haslo spelniajace wymagania: \nCo najmniej 8 znakow \nCo najmniej 1 cyfra \nCo najmniej 1 znak specjalny","Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else MessageBox.Show("Podaj dane", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ButtonReturn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
