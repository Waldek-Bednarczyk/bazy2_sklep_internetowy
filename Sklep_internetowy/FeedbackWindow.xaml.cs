﻿using System;
using System.Windows;
using System.Data;
using MySql.Data.MySqlClient;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for FeedbackWindow.xaml
    /// </summary>
    public partial class FeedbackWindow : Window
    {
        int _IDUser;
        public FeedbackWindow(int ID)
        {
            InitializeComponent();
            _IDUser = ID;
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxName.Text != "" && textBoxFeedback.Text != "")
            {
                try
                {
                    MySqlConnection conn = ((MainWindow)Application.Current.MainWindow).conn;
                    conn.Open();
                    string query = "SELECT ProduktID FROM produkty WHERE Nazwa_produktu = \"" + textBoxName.Text + "\"";
                    MySqlCommand comm = new MySqlCommand(query, conn);
                    MySqlDataAdapter adp = new MySqlDataAdapter(comm);
                    DataSet ds = new DataSet();
                    adp.Fill(ds, "LoadDataBinding");
                    conn.Close();
                    int produktID =Convert.ToInt32(ds.Tables[0].Rows[0]["ProduktID"].ToString());

                    ((MainWindow)Application.Current.MainWindow)._InternetowyEntities.Ocena_produktu(
                        _IDUser, produktID, textBoxFeedback.Text);
                    MessageBox.Show("Sukces", "Operacja zakończona powodzeniem", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Błąd", ex.InnerException.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
            }
            else MessageBox.Show("Błąd", "Podaj dane", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
