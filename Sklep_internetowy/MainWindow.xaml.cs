﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using MySql.Data;
using MySql.Data.EntityFramework;
using MySql.Data.MySqlClient;

namespace Sklep_internetowy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public sklep_internetowyEntities _InternetowyEntities = new sklep_internetowyEntities();
        public MySqlConnection conn = new MySqlConnection("host=localhost;database=sklep_internetowy;username=root;password=12tygrys12;");
        public int _ID;

        public MainWindow()
        {
            Login login = new Login();
            this.Hide();
            login.Show();
        }
        public MainWindow(int ID)
        {
            InitializeComponent();
            if (ID != -1)
            {
                _ID = ID;
                DeleteAccount.Visibility = Visibility.Collapsed;
                DeleteProduct.Visibility = Visibility.Collapsed;
                DeleteMagazin.Visibility = Visibility.Collapsed;
                DeleteEmployee.Visibility = Visibility.Collapsed;
                NewMagazin.Visibility = Visibility.Collapsed;
                DisplayComboBox.Visibility = Visibility.Collapsed;
                NewOrder.Visibility = Visibility.Collapsed;
            }
            else
            {
                displayUserComboBox.Visibility = Visibility.Collapsed;
                NewOrder.Visibility = Visibility.Collapsed;
                GiveFeedback.Visibility = Visibility.Collapsed;
            }
        }


        private void DisplayComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DeleteMagazin.Visibility = Visibility.Collapsed;

            if (DisplayComboBox.SelectedIndex == 0)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.adresy.ToList();
                TablesDataGrid.Columns[10].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[9].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[8].Visibility = Visibility.Collapsed;
            }
            if (DisplayComboBox.SelectedIndex == 1)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.kategorie.ToList();
                TablesDataGrid.Columns[2].Visibility = Visibility.Collapsed;
            }
            if (DisplayComboBox.SelectedIndex == 2)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.magazyny.ToList();
                TablesDataGrid.Columns[2].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[3].Visibility = Visibility.Collapsed;
                DeleteMagazin.Visibility = Visibility.Visible;
            }
            if (DisplayComboBox.SelectedIndex == 3)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.oceny.ToList();
                TablesDataGrid.Columns[2].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[3].Visibility = Visibility.Collapsed;
            }
            if (DisplayComboBox.SelectedIndex == 4)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.pracownicy.ToList();
                TablesDataGrid.Columns[7].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[8].Visibility = Visibility.Collapsed;
            }
            if (DisplayComboBox.SelectedIndex == 5)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.produkty.ToList();
                TablesDataGrid.Columns[6].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[7].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[8].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[9].Visibility = Visibility.Collapsed;
            }
            if (DisplayComboBox.SelectedIndex == 6)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.uzytkownicy.ToList();
                TablesDataGrid.Columns[6].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[7].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[8].Visibility = Visibility.Collapsed;
            }
            if (DisplayComboBox.SelectedIndex == 7)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.zamowienia.ToList();
                TablesDataGrid.Columns[5].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[6].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[7].Visibility = Visibility.Collapsed;
            }
            if (DisplayComboBox.SelectedIndex == 8)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.zamowienia_produkty.ToList();
                TablesDataGrid.Columns[3].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[4].Visibility = Visibility.Collapsed;
            }
        }

        private void Change_Password_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow();
            changePasswordWindow.Show();
        }

        private void TempButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            DeleteProductWindow deleteProductWindow = new DeleteProductWindow();
            deleteProductWindow.Show();
        }

        private void NewMagazin_Click(object sender, RoutedEventArgs e)
        {
            NewMagazinWindow newMagazinWindow = new NewMagazinWindow();
            newMagazinWindow.Show();
        }

        private void DeleteAccount_Click(object sender, RoutedEventArgs e)
        {
            DeleteAcoountWindow deleteAcoountWindow = new DeleteAcoountWindow();
            deleteAcoountWindow.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridRow dataGridRow = (DataGridRow)TablesDataGrid.ItemContainerGenerator.ContainerFromIndex(TablesDataGrid.SelectedIndex);
                TextBlock x = TablesDataGrid.Columns[1].GetCellContent(dataGridRow) as TextBlock;
                _InternetowyEntities.Usuniecie_magazynu(x.Text);
                TablesDataGrid.ItemsSource = _InternetowyEntities.magazyny;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd", ex.InnerException.Message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteEmployee_Click(object sender, RoutedEventArgs e)
        {
            DeleteEmployeeWindow deleteEmployee = new DeleteEmployeeWindow();
            deleteEmployee.Show();
        }

        private void NewOrder_Click(object sender, RoutedEventArgs e)
        {

            if(TablesDataGrid.SelectedItem != null)
            {
                DataGridRow dataGridRow = (DataGridRow)TablesDataGrid.ItemContainerGenerator.ContainerFromIndex(TablesDataGrid.SelectedIndex);
                TextBlock id = TablesDataGrid.Columns[0].GetCellContent(dataGridRow) as TextBlock;
                TextBlock price = TablesDataGrid.Columns[2].GetCellContent(dataGridRow) as TextBlock;
                NewOrderWindow newOrderWindow = new NewOrderWindow(Convert.ToInt32(id.Text), Convert.ToInt32(price.Text), _ID);
                newOrderWindow.Show();
            }
            else MessageBox.Show("Wybierz produkt", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void GiveFeedback_Click(object sender, RoutedEventArgs e)
        {
            FeedbackWindow feedbackWindow = new FeedbackWindow(_ID);
            feedbackWindow.Show();
        }

        private void DisplayUserComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NewOrder.Visibility = Visibility.Collapsed;
            if (displayUserComboBox.SelectedIndex == 0)
            {
                NewOrder.Visibility = Visibility.Visible;
                TablesDataGrid.ItemsSource = _InternetowyEntities.produkty.ToList();
                TablesDataGrid.Columns[6].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[7].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[8].Visibility = Visibility.Collapsed;
                TablesDataGrid.Columns[9].Visibility = Visibility.Collapsed;
            }
            if (displayUserComboBox.SelectedIndex == 1)
            {
                TablesDataGrid.ItemsSource = _InternetowyEntities.kategorie.ToList();
                TablesDataGrid.Columns[2].Visibility = Visibility.Collapsed;
            }
            if (displayUserComboBox.SelectedIndex == 2)
            {
                //zamowienia
                conn.Open();
                string query = "SELECT * FROM zamowienia WHERE UzytkownikID = " + _ID;
                MySqlCommand comm = new MySqlCommand(query, conn);
                MySqlDataAdapter adp = new MySqlDataAdapter(comm);
                DataSet ds = new DataSet();
                adp.Fill(ds, "LoadDataBinding");
                conn.Close();
                TablesDataGrid.ItemsSource = ds.Tables[0].DefaultView;
            }
            if (displayUserComboBox.SelectedIndex == 3)
            {
                //oceny
                conn.Open();
                string query = "SELECT * FROM oceny WHERE UzytkownikID = " + _ID;
                MySqlCommand comm = new MySqlCommand(query, conn);
                MySqlDataAdapter adp = new MySqlDataAdapter(comm);
                DataSet ds = new DataSet();
                adp.Fill(ds, "LoadDataBinding");
                conn.Close();
                TablesDataGrid.ItemsSource = ds.Tables[0].DefaultView;
            }
        }
    }
}
